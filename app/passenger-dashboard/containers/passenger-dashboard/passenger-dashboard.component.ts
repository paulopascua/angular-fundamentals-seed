import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Passenger } from '../../models/passenger.interface';

@Component({
  selector: 'passenger-dashboard',
  styleUrls: ['passenger-dashboard.component.scss'],
  template: `
    <div>
      <passenger-count
        [items]="passengers">
      </passenger-count>
      <passenger-detail
        *ngFor="let passenger of passengers;"
        [detail]="passenger"
        (edit)="handleEdit($event)"
        (remove)="handleRemove($event)"
        (view)="handleView($event)">
      </passenger-detail>
    </div>
  `
})

export class PassengerDashboardComponent implements OnInit {
  passengers: Passenger[];

  constructor(private router: Router) {}

  ngOnInit() {
    this.passengers = [{
        id: 1,
        fullName: 'Paulo',
        checkedIn: true,
        checkInDate: 1535599166000,
        children: [{ name: 'Diego', age: 22 }, { name: 'Josh', age: 21 }],
      }, {
        id: 2,
        fullName: 'Tom',
        checkedIn: false,
        checkInDate: null,
        children: [{ name: 'Jasper', age: 23 }],
      }, {
        id: 3,
        fullName: 'Kirzten',
        checkedIn: true,
        checkInDate: 1535858366000,
        children: null,
      },
    ];
  }

  handleEdit(event) {
    this.passengers = this.passengers.map((passenger: Passenger) => {
      if (passenger.id === event.id) {
        passenger = Object.assign({}, passenger, event);
      }
      return passenger;
    });
  }

  handleRemove(event: Passenger) {
    this.passengers = this.passengers.filter((passenger: Passenger) => {
      return passenger.id !== event.id;
    });
  }

  handleView(event: Passenger) {
    this.router.navigate(['/passengers', event.id]);
  }
}
