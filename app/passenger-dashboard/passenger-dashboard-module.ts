import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

// Container
import { PassengerDashboardComponent } from './containers/passenger-dashboard/passenger-dashboard.component';

// Components
import { PassengerCountComponent } from './components/passenger-count/passenger-count.component';
import { PassengerDetailComponent } from './components/passenger-detail/passenger-detail.component';
import { PassengerViewerComponent } from './components/passenger-viewer/passenger-viewer.component';

const routes: Routes = [
  {
    path: 'passengers',
    children: [
      { path: '', component: PassengerDashboardComponent },
      { path: ':id', component: PassengerViewerComponent }
    ]
  }
];

// Pass in the configuration object
@NgModule({
  // All components relative to this module
  declarations: [
    PassengerDashboardComponent,
    PassengerCountComponent,
    PassengerDetailComponent,
    PassengerViewerComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})

export class PassengerDashboardModule {}
